const express = require('express');


const router = express.Router();

const taskController = require("../controller/taskController");


router.get('/', (req,res)=>{
    
        taskController.getAllTasks().then(
            resultFromController => res.send(resultFromController)); 
});


router.get('/:id', (req,res)=>{
   
        taskController.getTaskById(req.params.id).then(
            resultFromController => res.send(resultFromController)); 
});



router.post("/", (req,res)=>{
    
    taskController.createTask(req.body).then(resultFromController =>{
        res.send(resultFromController);
    })
})


router.delete("/:id", (req,res)=>{
            
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

})


router.put("/:id", (req,res)=>{

    taskController.updateTask(req.params.id, req.body).then(resultFromController=> res.send(resultFromController))
})

router.put("/:id/:status", (req,res)=>{
    console.log(req.body);
    taskController.updateTaskStatus(req.params.id, req.params.status).then(resultFromController=> res.send(resultFromController))
})


module.exports = router;