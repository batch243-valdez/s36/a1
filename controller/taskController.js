

const { find } = require('../models/task');
const Task = require('../models/task');
const router = require('../routes/taskRoute');



module.exports.getAllTasks = ()=>{
    return Task.find({}).then(result =>{
        return result
    })
}

module.exports.getTaskById = (taskId)=>{
    return Task.findOne({taskId}).then(result =>{
        return result
    })
}



module.exports.createTask = (requestBody)=>{
    
    
    let newTask = new Task({
        name: requestBody.name
    })

    
    return newTask.save().then((task,error)=>{
        if(error){
            console.log(error);
            return false;
        } else{

           return task; 
        }
    })

    
}



module.exports.deleteTask = (taskId) =>{
                                                        
    return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
        if(err){
            console.log(err);
            return false;

        }
        else{
            return removedTask;
        }

    })
}
        

    
module.exports.updateTask = (taskId, newContent) =>{

return Task.findById(taskId).then((result,error)=>{

    if (error){
        console.log(error);
        return false;
    }
        result.name = newContent.name;

        return result.save().then((updatedTask, saveErr)=>{
            if (saveErr){
                console.log(saveErr)
                return false
            }
            else{
                return updatedTask
            }
        })
})
}

module.exports.updateTaskStatus = (taskId, taskStatus) =>{

return Task.findById(taskId).then((result,error)=>{

    if (error){
        console.log(error);
        return false;
    }
        result.status = taskStatus;

        return result.save().then((updatedTask, saveErr)=>{
            if (saveErr){
                console.log(saveErr)
                return false
            }
            else{
                return updatedTask
            }
        })
})
}