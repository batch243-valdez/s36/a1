
const express = require("express");
const mongoose = require("mongoose")

const app = express();
const port = 3001;

const taskRoute = require("./routes/taskRoute")

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/tasks", taskRoute)

app.listen(port, () => console.log(`Server running at port ${port}`));


mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.eiw7ahh.mongodb.net/B243-s36-activity?retryWrites=true&w=majority",
        {
            useNewUrlParser:true,
            useUnifiedTopology: true
        });